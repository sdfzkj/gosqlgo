package com.demo;

import java.util.Map;

import com.github.drinkjava2.gosqlgo.GoSqlGoEnv;
import com.github.drinkjava2.gosqlgo.util.GsgFileUtils;

@SuppressWarnings("all")
public class ScriptTemplate extends BaseTemplate {
	private static String scriptSrc;
	static {
		/* GSG BODY BEGIN */
		String scriptSrc = "";
		String className = "ScriptTemplate";
		/* GSG BODY END */
		try {
			GsgFileUtils.writeFile(GoSqlGoEnv.getClassesDeployFolder() + "/" + className + ".js", scriptSrc, "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object executeBody() {
		Map<String, String> paramMap = this.getParamMap();
		paramMap.put("$0", this.getClass().getSimpleName());
		return httpPostOnURL(GoSqlGoEnv.getNodeJsUrl(), paramMap);
	}

}
